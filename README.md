# Git Tutorial

In deze tutorial leer je over Git version control. Dit mechanisme wordt in de software development wereld bijna overal gebruikt om teams te laten samenwerken aan projecten en verschillende codeversies te beheren.

Deze tutorial brengt je de basis van Git bij, en leert je termen als clone, merge, push, pull en branch. Na een korte introductie zul je zelf aan de slag gaan om de Calco Favorites-lijst van films en/of series verder te ontwikkelen. Het is een korte en eenvoudige introductie, die hopelijk voldoende basis biedt om er zelf mee aan de slag te gaan. We raden je aan ook voor andere projecten en deepdives binnen Calco zoveel mogelijk met git te werken. Zo ben je goed voorbereid voor een technische inzet waar je waarschijnlijk ook veelvuldig met Git in aanraking zult komen.

## Over Git
[Git](https://en.wikipedia.org/wiki/Git) wordt gebruikt om samen te werken aan applicaties en verschillende versies van de code bij te houden. Dat gebeurt in een zogenaamde repository. In moderne software wordt dit meestal gecombineerd met een [CI/CD Pipeline](https://semaphoreci.com/blog/cicd-pipeline) zodat de code die je schrijft direct in productie genomen kan worden.

Er zijn verschillende aanbieders van Git-software. De twee meest bekende aanbieders zijn [GitHub](https://github.com/) en [GitLab](https://gitlab.com/), maar ook in bijvoorbeeld [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/) vind je een gitsysteem. Bij Calco maken we gebruik van GitLab.

Meer lezen? Op Sharepoint vind je het boek [GitHub for Dummies](https://calcobv.sharepoint.com/:b:/r/sites/masterclass/Ebooks/Dummies%20-%20GitHub.pdf?csf=1&web=1&e=c7Lnyo).

## De Git workflow

De basisprincipes van git zijn erg eenvoudig en zul je in iedere enigszins technische IT-functie tegenkomen. De basis van elke respitory is de 'master branch', de plek waar de hoofdversie van de code bijgehouden wordt. Vaak bestaat er naast de master branch een development branch, een tak die gebruikt wordt om de verschillende aanvullingen aan de code samen te brengen voordat er een nieuwe versie uitgebracht wordt.

Naast de master en eventueel development branch, kent elke repository meerdere 'feature branches'. Dit zijn vertakkingen van de code die bedoeld zijn voor ��n specifieke functie. Hier doe je als (groep) developer(s) je ontwikkelwerk voordat je het met de hoofdcode op de master- of development-branch 'merged'. Git kent 'local branches' en 'remote branches'. Een local branch is aanwezig op jouw computer, een remote branch staat in de centrale repository in de cloud. Meestal zijn deze aan elkaar gekoppeld.

Het proces van nieuwe code schrijven en uploaden bestaat uit vier stappen. Afhankelijk van de status van je lokale code en of je jouw wijzigingen al online wilt zetten kun je die deels overslaan.

1. 'Fetch': met een fetch haal je de laatste versie-informatie van de server op. Zo kun je zien of jouw lokale code up-to-date is en wat er gewijzigd is.
2. 'Pull': met een pull download je de laatste code naar je lokale ontwikkelomgeving. Je haalt dus data op van de remote branch naar je local branch.
3. 'Commit': met een commit sla je je wijzigingen op in de repository. Om duidelijk te maken wat je veranderd hebt, schrijf je bij elke commit een kort berichtje wat er gewijzigd is.
4. 'Push': met een push upload je de laatste commits naar de server. Je brengt dus de data van je local branch naar de bijbehorende remote branch.

Verder is het goed om te weten dat het bestand .gitignore gebruikt kan worden om bestanden aan te geven die wel in de lokale map van de repository staan, maar niet in de repository terug moeten komen.

Je zult deze theorie nu in de praktijk brengen met een korte opdracht.

## Getting started
We werken bij Calco met GitLab. Je hebt om deze tutorial te volgen dan ook een GitLab account nodig; die kun je hier aanmaken: https://gitlab.com/users/sign_up. De tutorial is gebaseerd op Visual Studio, wat standaard op je Calco-laptop ge�nstalleerd is.

Je begint met het 'clonen' van de respository. Zo maak je de repository beschikbaar op je eigen systeem. Om dit te doen open je Visual Studio en zoek je de Team Explorer (meestal rechts in beeld, anders te vinden via View -> Team Explorer). Klik onder Local Git Repositories op clone en voer deze URL in: https://gitlab.com/systeembeheer.calco/gitmanual.git

Het systeem zal je vragen om de gebruikersnaam en wachtwoord van je GitLab account; als je die ingevuld hebt wordt de repository als het goed is beschikbaar onder het kopje Local Git Repositories. Door hier te dubbelklikken op een repository kun je hem openen.

## Je eerste branch
Nu is het tijd om een eigen feature branch te maken waar je je wijzigingen op gaat aanmaken. Je actieve branch zie je altijd rechtsonderin je scherm (3 in de afbeelding). Als je hierop klikt kun je op new branch klikken, waarna je in het volgende scherm komt.

![alt text](https://gitlab.com/systeembeheer.calco/gitmanual/-/raw/master/Images/GitOverzichtVS.png "Git Branches")

Hier zie je een overzicht van de beschikbare branches in de repostory (1) en kun je een naam voor de nieuwe branch ingeven (2). Door het vinkje 'checkout branch' aan te zetten kun je de branch direct bekijken nadat hij aangemaakt is. Zo heb je je eigen feature branch gemaakt waarop je kunt beginnen te werken.

In de solution explorer van Visual Studio kun je naar het bestand gaan dat je wilt wijzigen. In dit geval is het de bedoeling dat we de Calco Favorites lijst van films en/of series updaten. Ga dus naar het mapje CalcoFavorites en open 'series.txt' of 'films.txt'.

Voer hier je favoriete films en/of series toe, zorg ervoor dat de categorisering beter wordt, schoon de lijst op of start een extra lijst. Leef je uit! Zorg in ieder geval dat je minimaal ��n nuttige toevoeging aan een van de bestanden doet.

Via de verschillende opties van de Team Explorer kun je nu je wijzigingen bekijken, commiten en synchroniseren. Zie de afbeelding hieronder voor de verschillende onderdelen van de Team Explorer.

1. Hier kun je de wijzigingen die hebt aangebracht sinds de vorige commit bekijken. Je kunt hier ook een nieuwe commit maken, en eventueel wijzigingen ongedaan maken.
2. Hier kun je de repo synchroniseren. Via de knop 'fetch' haal je de laatste wijzigingen op, met 'pull' kun je nieuwe commits downloaden en met 'push' kun je nieuwe commits uploaden.
3. Hier zie je een overzicht van alle branches (remote en local) in de repository
4. Hier kun je 'tags' vinden. Een tag is een opgeslagen punt in de geschiedenis van de code. Zo kun je bijvoorbeeld voor elke release een tag maken, zodat je altijd de versie van de code die ooit uitgebracht is kunt terugvinden.

![alt text](https://gitlab.com/systeembeheer.calco/gitmanual/-/raw/master/Images/TeamExplorerFuncties.png "Team Explorer features")

Als je klaar bent met het aanbrengen van wijzigingen en al je commits hebt gemaakt, kun je via het het kopje 'Sync' je commits pushen. Deze komen dan in de remote branch terecht.

## Merge requests

Nu je klaar bent met je wijzigingen is het tijd om een 'merge request' (ook wel bekend als 'pull request') te doen. Hiermee doe je een aanvraag om de wijzigingen van jouw feature branch in de master of development branch aan te brengen. Je doet dan een 'merge naar master'. Het is gebruikelijk om een feature branch na het afronden van een merge request te verwijderen.

GitLab heeft een speciaal dashboard voor merge requests. Voor deze repository is dat de volgende pagina: https://gitlab.com/systeembeheer.calco/gitmanual/-/merge_requests. Ga hierheen, druk op new merge request en selecteer je branch. Je kunt ook via het overzicht van branches een merge request aanmaken.

Zorg dat je hier in ieder geval een duidelijke naam en omschrijving voor je merge request invoert. Zorg er ook voor dat je feature branch verwijderd wordt na de goedkeuring van het merge request, om de repository schoon te houden. Alle andere instellingen zijn optioneel. Als je klaar bent kun je het merge request aanmaken.

Meestal wordt een check door een bepaalde groep onderhouders van de code afgedwongen als je een merge request naar de master branch doet. Om deze tutorial laagdrempelig te houden is dat nu niet het geval, maar het is wel de bedoeling dat je je merge request in ieder geval door een medetrainee laat checken en goedkeuren. Dit is een belangrijk mechanisme om de kwaliteit van de code hoog te houden. Vraag dus een medetrainee om naar jouw merge request te kijken.

## Over merge conflicts, rebasen en mergen van master

Het kan gebeuren dat iemand anders in een andere feature branch aan hetzelfde bestand heeft gewerkt en wijzigingen heeft gemaakt die conflicteren met jouw wijzigingen. Als die feature branch eerder gemerged is naar master, dan kun je een zogenaamd merge conflict krijgen. Git weet dan niet hoe het de wijzigingen goed samen moet voegen. Geen paniek, dit is heel gebruikelijk!

Om dit op te lossen kun je een 'merge van master' doen of je branch rebasen. Dat komt in grote lijnen op hetzelfde neer, maar de praktijk leert dat een merge van master wat minder foutgevoelig is. Klik hiervoor met je rechter muisknop op je branch en selecteer 'merge from....' en dan master.

Je zult nu een foutmelding krijgen dat er merge conflicts zijn die je moet oplossen, waarbij je de conflicten een voor een kunt behandelen. Kies per merge conflict of je het bestand op de server of het lokale bestand wilt behouden, of dat je de bestanden wilt mergen. In dat laatste geval kun je per conflicterende regel code kiezen of de lokale variant of de variant op de server behouden moet worden.

## Tot slot
Deze introductie heeft je hopelijk een gevoel gegeven voor wat Git is, wat je eraan hebt en hoe je ermee moet werken. Als je meer te weten wilt komen over hoe je werkt met Git, raden we je aan een eigen project (bijvoorbeeld de Java Deepdive) in een zelfgemaakte repository te zetten en daar lekker mee aan de slag te gaan. Het is niet moeilijk, maar wel een handige ervaring om te hebben. Google of je medetrainees zijn je vrienden bij eventuele problemen.